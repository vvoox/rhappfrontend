import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./Componenets/home/home.component";
import {SalariesComponent} from "./Componenets/salaries/salaries.component";
import {RetraitesComponent} from "./Componenets/retraites/retraites.component";
import {AbsencesComponent} from "./Componenets/absences/absences.component";
import {AvantagesComponent} from "./Componenets/avantages/avantages.component";
import {AddRetraiteComponent} from "./Componenets/add-retraite/add-retraite.component";
import {ProfileComponent} from "./Componenets/profile/profile.component";


const routes: Routes = [
  {
    path:"home",
    component : HomeComponent
  },
  {
    path:"salaries",
    component:SalariesComponent
  },
  {
    path:"retraites",
    component:RetraitesComponent
  },
  {
    path:"absences",
    component:AbsencesComponent
  },
  {
    path:"avantages",
    component:AvantagesComponent
  },
  {
    path:"addRetraite",
    component:AddRetraiteComponent
  },
  {
    path:"profile",
    component:ProfileComponent
  },
  {
    path: '', redirectTo: '/home', pathMatch:'full'

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
