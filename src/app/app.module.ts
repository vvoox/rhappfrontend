import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './Componenets/home/home.component';
import { RetraitesComponent } from './Componenets/retraites/retraites.component';
import { AbsencesComponent } from './Componenets/absences/absences.component';
import { SalariesComponent } from './Componenets/salaries/salaries.component';
import { AvantagesComponent } from './Componenets/avantages/avantages.component';
import { AddRetraiteComponent } from './Componenets/add-retraite/add-retraite.component';
import { ProfileComponent } from './Componenets/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RetraitesComponent,
    AbsencesComponent,
    SalariesComponent,
    AvantagesComponent,
    AddRetraiteComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
