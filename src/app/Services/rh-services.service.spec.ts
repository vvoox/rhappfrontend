import { TestBed } from '@angular/core/testing';

import { RhServicesService } from './rh-services.service';

describe('RhServicesService', () => {
  let service: RhServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RhServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
