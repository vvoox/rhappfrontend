export class Salaries{

  public id:number;
  public numSomme:number;
  public cin:string;
  public telephone:string;
  public adresse:string;
  public image:string
  public dateNaissance:Date;
  public lieuNaissance:string;
  public dateAffectation:Date;
  public dateCreation:Date;
  public dateUpdate:Date;
  public diplomeObt:string;
  public fonction:string;
  public solde:number;
  public nomUrg:string;
  public prenomUrg:string;
  public adresseUrg:string;
  public cinUrg:string;
  public emailUrg:string;


}
