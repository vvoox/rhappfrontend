import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRetraiteComponent } from './add-retraite.component';

describe('AddRetraiteComponent', () => {
  let component: AddRetraiteComponent;
  let fixture: ComponentFixture<AddRetraiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRetraiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRetraiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
