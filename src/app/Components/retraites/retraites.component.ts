import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-retraites',
  templateUrl: './retraites.component.html',
  styleUrls: ['./retraites.component.css']
})
export class RetraitesComponent implements OnInit {

  constructor( private router:Router) { }

  ngOnInit(): void {
  }

  addRetraite(){
    this.router.navigateByUrl("/addRetraite");
  }

}
